import Sequelize from "sequelize";
import sequelize from '../configs/db-config.js';

const Poids = sequelize.define(
  'Poids', 
    {
      Date: { type: Sequelize.DATE, allowNull: false },
      Poids: { type: Sequelize.FLOAT, allowNull: false },
      IMC_ID: { type: Sequelize.INTEGER, allowNull: true },
      IMC_Cal: { type: Sequelize.FLOAT, allowNull: true },
      Ecart_Poids: { type: Sequelize.FLOAT, allowNull: true },
      Ecart_Cumul: { type: Sequelize.FLOAT, allowNull: true },
      IMG_Graisse: { type: Sequelize.FLOAT, allowNull: true },
      IMG_Hydrat: { type: Sequelize.FLOAT, allowNull: true },
      IMG_Muscle: { type: Sequelize.FLOAT, allowNull: true },
    },
  )

  Poids.associate = function(models) {
    // associations go here
};

// create table with user model
Poids.sync()
    .then(() => console.log('Users table Poids created successfully'))
    .catch(err => console.log('oooh, did you enter wrong database credentials?'));

export default Poids;
import express from 'express';
const router = express.Router();

import { getAllGlycemies, getGlycemieById, createGlycemie, updateGlycemie, deleteGlycemie } from '../controllers/glycemie.controller.js';
<<<<<<< refs/remotes/origin/main
=======
import { createImc, deleteImc, getAllImcs, getImcById, updateImc } from '../controllers/imc.controller.js';
import { getAllImg, getImgById, createImg, updateImg, deleteImg } from '../controllers/img.controller.js';
import { createParam, deleteParam, getAllParams, getParamById, updateParam } from '../controllers/params.controller.js';
import { getAllPoids, getPoidsById, createPoids, updatePoids, deletePoids } from '../controllers/poids.controller.js';
>>>>>>> Mise en place


// Glycemie
router.get('/glycemies', getAllGlycemies);
router.get('/glycemies/:id', getGlycemieById);
router.post('/glycemies', createGlycemie);
router.put('/glycemies/:id', updateGlycemie);
router.delete('/glycemies/:id', deleteGlycemie);

<<<<<<< refs/remotes/origin/main
=======
// Img
router.get('/imgs', getAllImg);
router.get('/imgs/:id', getImgById);
router.post('/imgs', createImg);
router.put('/imgs/:id', updateImg);
router.delete('/imgs/:id', deleteImg);

// Imc
router.get('/imcs', getAllImcs);
router.get('/imcs/:id', getImcById);
router.post('/imcs', createImc);
router.put('/imcs/:id', updateImc);
router.delete('/imcs/:id', deleteImc);

// Params
router.get('/params', getAllParams);
router.get('/params/:id', getParamById);
router.post('/params', createParam);
router.put('/params/:id', updateParam);
router.delete('/params/:id', deleteParam);

// Poids
router.get('/poids', getAllPoids);
router.get('/poids/:id', getPoidsById);
router.post('/poids', createPoids);
router.put('/poids/:id', updatePoids);
router.delete('/poids/:id', deletePoids);

>>>>>>> Mise en place
export default router;
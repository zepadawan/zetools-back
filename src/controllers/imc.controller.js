import { getAll, getById, create, update, _delete } from '../services/Img.service.js';

export function getAllImcs(req, res, next) {
  return getAll(req, res)
    .then((imcs) => res.json({
      args: imcs,
      status: 200
    }))
    .catch(next);

}

export function getImcById(req, res, next) {
  return getById(req.params.id, res)
    .then((imc) => res.json({
      args: imc,
      status: 200
    }))
    .catch(next);
}

export function createImc(req, res, next) {
  return create(req.body, res)
    .then(() => res.json({
      imc: req.body,
      message: 'Registration successful',
      status: 201
    }))
    .catch(next);
}

export function updateImc(req, res, next) {
  return update(req.params.id, req.body)
    .then((imc) => res.json({
      imc: req.body,
      message: 'Moification successsful',
      status: 202
    }))
    .catch(next);
}

export function deleteImc(req, res, next) {
  return _delete(req.params.id, res)
    .then(imc => res.json({
      imc_numero: req.params.id,
      status: 203,
      message: `Imc is deleted`
    }))
    .catch(next);
}



export default {};
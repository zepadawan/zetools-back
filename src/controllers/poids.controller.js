import { getAll, getById, create, update, _delete } from '../services/Poids.service.js';

export function getAllPoids(req, res, next) {
  return getAll(req, res)
    .then((poids) => res.json({
      args: poids,
      status: 200
    }))
    .catch(next);
}

export function getPoidsById(req, res, next) {
  return getById(req.params.id, res)
    .then((poids) => res.json({
      args: poids,
      status: 200
    }))
    .catch(next);
}

export function createPoids(req, res, next) {
  return create(req.body, res)
    .then(() => res.json({
      poids: req.body,
      message: 'Registration successful',
      status: 201
    }))
    .catch(next);
}

export function updatePoids(req, res, next) {
  return update(req.params.id, req.body)
    .then((poids) => res.json({
      poids: req.body,
      message: 'Moification successsful',
      status: 202
    }))
    .catch(next);
}

export function deletePoids(req, res, next) {
  return _delete(req.params.id, res)
    .then(poids => res.json({
      poids_numero: req.params.id,
      status: 203,
      message: `Img is deleted`
    }))
    .catch(next);
}



export default {};
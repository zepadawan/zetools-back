import { getAll, getById, create, update, _delete } from '../services/Params.service.js';

export function getAllParams(req, res, next) {
  return getAll(req, res)
    .then((params) => res.json({
      args: params,
      status: 200
    }))
    .catch(next);

}

export function getParamById(req, res, next) {
  return getById(req.params.id, res)
    .then((param) => res.json({
      args: param,
      status: 200
    }))
    .catch(next);
}

export function createParam(req, res, next) {
  return create(req.body, res)
    .then(() => res.json({
      param: req.body,
      message: 'Registration successful',
      status: 201
    }))
    .catch(next);
}

export function updateParam(req, res, next) {
  return update(req.params.id, req.body)
    .then((param) => res.json({
      param: req.body,
      message: 'Moification successsful',
      status: 202
    }))
    .catch(next);
}

export function deleteParam(req, res, next) {
  return _delete(req.params.id, res)
    .then(img => res.json({
      param_numero: req.params.id,
      status: 203,
      message: `Param is deleted`
    }))
    .catch(next);
}



export default {};
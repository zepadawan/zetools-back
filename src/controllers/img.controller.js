import { getAll, getById, create, update, _delete } from '../services/Img.service.js';

export function getAllImg(req, res, next) {
  return getAll(req, res)
    .then((imgs) => res.json({
      args: imgs,
      status: 200
    }))
    .catch(next);

}

export function getImgById(req, res, next) {
  return getById(req.params.id, res)
    .then((img) => res.json({
      args: img,
      status: 200
    }))
    .catch(next);
}

export function createImg(req, res, next) {
  return create(req.body, res)
    .then(() => res.json({
      img: req.body,
      message: 'Registration successful',
      status: 201
    }))
    .catch(next);
}

export function updateImg(req, res, next) {
  return update(req.params.id, req.body)
    .then((img) => res.json({
      img: req.body,
      message: 'Moification successsful',
      status: 202
    }))
    .catch(next);
}

export function deleteImg(req, res, next) {
  return _delete(req.params.id, res)
    .then(img => res.json({
      img_numero: req.params.id,
      status: 203,
      message: `Img is deleted`
    }))
    .catch(next);
}



export default {};
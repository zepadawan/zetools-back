import IMC from '../models/IMC.model.js';

export async function getAll() {
  return await IMC.findAll();
}

export function getById(id) {
  return getImc(id);
}

export function create(req) {
  return IMC.create(req);
}

export async function update(id, params) {
  return await IMC.update(params, { where: { id: id } });
}

export async function _delete(id) {
  const imc = await getImc(id);
  await imc.destroy();
}

// helper functions
async function getImc(id) {
  const imc = await IMC.findByPk(id);
  if (!imc) throw 'Imc not found';
  return imc;
}

export default {}



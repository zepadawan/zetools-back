import Glycemie from '../models/Glycemie.model.js';

export async function getAll() {
  return await Glycemie.findAll();
}

export function getById(id) {
  return getGlycemie(id);
}

export function create(req) {
  return Glycemie.create(req);
}

export async function update(id, params) {
  return await Glycemie.update(params, { where: { id: id } });
}

export async function _delete(id) {
  const glycemie = await getGlycemie(id);
  await glycemie.destroy();
}

// helper functions
async function getGlycemie(id) {
  const glycemie = await Glycemie.findByPk(id);
  if (!glycemie) throw 'Glycemie not found';
  return glycemie;
}

export default {}



import Poids from '../models/Poids.model.js';

export async function getAll() {
  return await Poids.findAll();
}

export function getById(id) {
  return getPoids(id);
}

export function create(req) {
  return Poids.create(req);
}

export async function update(id, params) {
  return await Poids.update(params, { where: { id: id } });
}

export async function _delete(id) {
  const poids = await getPoids(id);
  await poids.destroy();
}

// helper functions
async function getPoids(id) {
  const poids = await Poids.findByPk(id);
  if (!poids) throw 'poids not found';
  return poids;
}

export default {}



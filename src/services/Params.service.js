import Params from '../models/Params.model.js';

export async function getAll() {
  return await Params.findAll();
}

export function getById(id) {
  return getParams(id);
}

export function create(req) {
  return Params.create(req);
}

export async function update(id, params) {
  return await Params.update(params, { where: { id: id } });
}

export async function _delete(id) {
  const params = await getParams(id);
  await params.destroy();
}

// helper functions
async function getParams(id) {
  const params = await Params.findByPk(id);
  if (!params) throw 'params not found';
  return params;
}

export default {}



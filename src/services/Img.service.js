import IMG from '../models/IMG.model.js';

export async function getAll() {
  return await IMG.findAll();
}

export function getById(id) {
  return getImg(id);
}

export function create(req) {
  return IMG.create(req);
}

export async function update(id, params) {
  return await IMG.update(params, { where: { id: id } });
}

export async function _delete(id) {
  const img = await getImg(id);
  await img.destroy();
}

// helper functions
async function getImg(id) {
  const img = await IMG.findByPk(id);
  if (!img) throw 'Img not found';
  return img;
}

export default {}


